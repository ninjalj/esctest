import tests.decrectops
import esccmd
from escutil import knownBug, onlyIn, vtFirmwareVersion

CHARACTER = "%"

class DECFRATests(tests.decrectops.FillRectangleTests):
  def fill(self, top=None, left=None, bottom=None, right=None, character=ord(CHARACTER)):
    esccmd.DECFRA(character, top, left, bottom, right)

  def characters(self, point, count):
    return CHARACTER * count

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_basic(self):
    self.fillRectangle_basic()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_invalidRectDoesNothing(self):
    self.fillRectangle_invalidRectDoesNothing()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_defaultArgs(self):
    self.fillRectangle_defaultArgs()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_defaultCharacterArgIsSpace(self):
    self.fillRectangle_defaultCharacterArgIsSpace(None)

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_zeroCharacterArgIsSpace(self):
    self.fillRectangle_defaultCharacterArgIsSpace(0)

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_respectsOriginMode(self):
    self.fillRectangle_respectsOriginMode()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_overlyLargeSourceClippedToScreenSize(self):
    self.fillRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_cursorDoesNotMove(self):
    self.fillRectangle_cursorDoesNotMove()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_ignoresMargins(self):
    self.fillRectangle_ignoresMargins()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_outsideMarginsInOriginMode(self):
    self.fillRectangle_outsideMarginsInOriginMode()

  @onlyIn(terminal="vte", reason="vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_characterFromBMP(self):
    self.fillRectangle_characterFromBMP()

  @onlyIn(terminal="vte", reason="vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_characterFromSMP(self):
    self.fillRectangle_characterFromSMP()

  @onlyIn(terminal="vte", reason="vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_characterFromSMPAsSurrogatesPair(self):
    self.fillRectangle_characterFromSMPAsSurrogatesPair()

  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_characterFromGL(self):
    self.fillRectangle_characterFromGL()

  @knownBug(terminal="vte", reason="GR not supported")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECFRA_characterFromGR(self):
    self.fillRectangle_characterFromGR()
