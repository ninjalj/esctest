import tests.decrectopschange
import esccmd
from escutil import knownBug, onlyIn, vtFirmwareVersion

class DECCARATests(tests.decrectopschange.ChangeRectangleTests):

  def __init__(self, decsace):
    tests.decrectopschange.ChangeRectangleTests.__init__(self, decsace)
    self.selftest()

  def change(self, top=None, left=None, bottom=None, right=None, sgr=None):
    esccmd.DECSACE(self.decsace())
    esccmd.DECCARA(top, left, bottom, right, sgr)
    esccmd.DECSACE(0)

  def compute_unchanged(self, attr, changes):
    """ Compute attributes for unchanged cells."""
    required=[]
    disallowed=[]
    for set, reset in esccmd.ALL_SGR:
      if set in attr:
        required += [set]
      elif set in changes:
        disallowed += [set]

    return [required, disallowed]

  def compute_changes(self, attr, changes):
    """ Compute attributes for changed cells. Note this doesn't support
    SGR 30...37, 40...47."""
    required=[code for code in attr if esccmd.SGRCodeIsSet(code)]
    disallowed=[]

    for set, reset in esccmd.ALL_SGR:
      if set in changes:
        if not set in required:
          required += [set]
        if set in disallowed:
          disallowed.remove(set)

      if reset in changes:
        if reset == esccmd.SGR_RESET:
          disallowed += [code for code in required if not code in disallowed]
          required=[]
        elif set in required:
          required.remove(set)
          disallowed += [set]

    return [required, disallowed]

  def selftest(self):
    attr = [esccmd.SGR_BOLD]
    changes = [esccmd.SGR_UNDERLINE, esccmd.SGR_RESET_BOLD]
    assert self.compute_unchanged(attr, changes) == [attr, [esccmd.SGR_UNDERLINE]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_UNDERLINE], attr]

    attr = [esccmd.SGR_RESET]
    changes = [esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE, esccmd.SGR_BLINK, esccmd.SGR_INVERSE]
    assert self.compute_unchanged(attr, changes) == [[], changes]
    assert self.compute_changes(attr, changes) == [changes, []]

    attr = [esccmd.SGR_RESET]
    changes = [esccmd.SGR_RESET_BOLD, esccmd.SGR_ITALIC, esccmd.SGR_UNDERLINE_DOUBLE]
    assert self.compute_unchanged(attr, changes) == [[], [esccmd.SGR_ITALIC, esccmd.SGR_UNDERLINE_DOUBLE]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_ITALIC, esccmd.SGR_UNDERLINE_DOUBLE], []]

    attr = [esccmd.SGR_RESET]
    changes = [esccmd.SGR_BOLD]
    assert self.compute_unchanged(attr, changes) == [[], [esccmd.SGR_BOLD]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_BOLD], []]

    attr = [esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE]
    changes = [esccmd.SGR_BOLD, esccmd.SGR_RESET_BOLD]
    assert self.compute_unchanged(attr, changes) == [[esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE],[]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_UNDERLINE], [esccmd.SGR_BOLD]]

    attr = [esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE]
    changes = [esccmd.SGR_RESET]
    assert self.compute_unchanged(attr, changes) == [[esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE], []]
    assert self.compute_changes(attr, changes) == [[], [esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE]]

class DECCARARectangleTests(DECCARATests):

  def __init__(self):
    DECCARATests.__init__(self, decsace=esccmd.DECSACE_RECTANGLE)

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_basic(self):
    self.changeRectangle_basic()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_supportedAttributes(self):
    self.changeRectangle_supportedAttributes()

  @onlyIn("vte", "vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_supportedAttributesExtension(self):
    self.changeRectangle_supportedAttributesExtension()

  @onlyIn("vte", "vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_supportedAttributesExtensionColors(self):
    self.changeRectangle_supportedAttributesExtensionColors()

  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_attributeSetAndReset(self):
    self.changeRectangle_attributeSetAndReset()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECCARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_invalidRectDoesNothing(self):
    self.changeRectangle_invalidRectDoesNothing()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_defaultArgs(self):
    self.changeRectangle_defaultArgs()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_SGRZeroResets(self):
    self.changeRectangle_SGRZeroResets()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_respectsOriginMode(self):
    self.changeRectangle_respectsOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_overlyLargeSourceClippedToScreenSize(self):
    self.changeRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECCARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_cursorDoesNotMove(self):
    self.changeRectangle_cursorDoesNotMove()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_ignoresMargins(self):
    self.changeRectangle_ignoresMargins()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_outsideMarginsInOriginMode(self):
    self.changeRectangle_outsideMarginsInOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  #@knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_unoccupiedCells(self):
    self.changeRectangle_unoccupiedCells()

class DECCARAStreamTests(DECCARATests):

  def __init__(self):
    DECCARATests.__init__(self, decsace=esccmd.DECSACE_STREAM)

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_basic(self):
    self.changeRectangle_basic()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_supportedAttributes(self):
    self.changeRectangle_supportedAttributes()

  @onlyIn("vte", "vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_supportedAttributesExtension(self):
    self.changeRectangle_supportedAttributesExtension()

  @onlyIn("vte", "vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_supportedAttributesExtensionColors(self):
    self.changeRectangle_supportedAttributesExtensionColors()

  @onlyIn("vte", "vte extension")
  def test_DECCARA_attributeSetAndReset(self):
    self.changeRectangle_attributeSetAndReset()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECCARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_invalidRectDoesNothing(self):
    self.changeRectangle_invalidRectDoesNothing()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_defaultArgs(self):
    self.changeRectangle_defaultArgs()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_SGRZeroResets(self):
    self.changeRectangle_SGRZeroResets()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_respectsOriginMode(self):
    self.changeRectangle_respectsOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_overlyLargeSourceClippedToScreenSize(self):
    self.changeRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECCARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_cursorDoesNotMove(self):
    self.changeRectangle_cursorDoesNotMove()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_ignoresMargins(self):
    self.changeRectangle_ignoresMargins()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECCARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_outsideMarginsInOriginMode(self):
    self.changeRectangle_outsideMarginsInOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECCARA_unoccupiedCells(self):
    self.changeRectangle_unoccupiedCells()
