from esc import CR, LF
import esccmd
import escio
from escutil import AssertEQ
from escutil import AssertScreenCharsInRectEqual
from escutil import AssertCellMatchesSGRParams
from escutil import AssertRectMatchesSGRParams
from escutil import GetCursorPosition
from escutil import GetScreenSize
from escutil import knownBug
from escutil import vtLevel
from esctypes import Point, Rect, CellStream

class ChangeRectangleTests(object):

  def __init__(self, decsace):
    self._decsace = decsace

  def decsace(self):
    return self._decsace

  def data(cls, width=8, height=8):
    return ["a" * width] * height

  def datarect(self):
    return Rect(1, 1, 8, 8)

  def changerect(self, left, top, right, bottom):
    if self._decsace == 2:
      return Rect(left, top, right, bottom)
    else:
      return CellStream(self.datarect(), left, top, right, bottom)

  def prepare(self):
    esccmd.CUP(Point(1, 1))
    for line in self.data():
      escio.Write(line + CR + LF)

  def change(self, top=None, left=None, bottom=None, right=None, sgr=None):
    """Subclasses should override this to do the appropriate change action."""
    pass

  @vtLevel(4)
  def changeRectangle_basic(self):
    attr = esccmd.SGR(esccmd.SGR_BOLD)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_UNDERLINE, esccmd.SGR_RESET_BOLD]
    self.change(top=5,
                left=5,
                bottom=7,
                right=7,
                sgr=attrchange)

    # No character changes should occur
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 7, 7),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_supportedAttributes(self):
    """ According to the DEC documentation, the supported attributes
    are bold, underline, blink and inverse, with all others to be
    ignored unless they are part of a well-defined extension.
    Test that all of these are supported, but don't test for others
    not to be supported since there may be an extension (like in vte).
    """

    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE, esccmd.SGR_BLINK, esccmd.SGR_INVERSE]
    self.change(top=5,
                left=5,
                bottom=7,
                right=7,
                sgr=attrchange)

    # No character changes should occur
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 7, 7),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_supportedAttributesExtension(self):
    """ According to the DEC documentation, the supported attributes
    are bold, underline, blink and inverse, with all others to be
    ignored unless they are part of a well-defined extension.
    E.g. vte has such an extension by allowing any SGR attribute in
    a DECCARA and all non-colour attributes in a DECRARA.
    Test some of the non-DEC attributes.
    """

    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_RESET_BOLD, esccmd.SGR_ITALIC, esccmd.SGR_STRIKETHROUGH]
    self.change(top=5,
                left=5,
                bottom=7,
                right=7,
                sgr=attrchange)

    # No character changes should occur
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 7, 7),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_supportedAttributesExtensionColors(self):
    """ According to the DEC documentation, the supported attributes
    are bold, underline, blink and inverse, with all others to be
    ignored unless they are part of a well-defined extension.
    E.g. vte has such an extension by allowing any SGR attribute in
    a DECCARA and all non-colour attributes in a DECRARA.
    Test the colour attributes.
    """

    attr = esccmd.SGR(esccmd.SGR_RESET, 31, 42)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_RESET_FOREGROUND, 43]
    self.change(top=5,
                left=5,
                bottom=7,
                right=7,
                sgr=attrchange)

    # No character changes should occur
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 7, 7),
                               [[31, 42], [43]],
                               [[43], [42]])


  @vtLevel(4)
  def changeRectangle_attributeSetAndReset(self):
    """Use both the SGR set and the reset for the same attribute in the change."""

    attr = esccmd.SGR(esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_BOLD, esccmd.SGR_RESET_BOLD]
    self.change(top=5,
                left=5,
                bottom=7,
                right=7,
                sgr=attrchange)

    # No character changes should occur
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 7, 7),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_invalidRectDoesNothing(self):
    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_BOLD]
    self.change(top=5,
                left=5,
                bottom=4,
                right=4,
                sgr=attrchange)
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               Rect(),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_defaultArgs(self):
    """Write a value at each corner, run change with default rect args, and verify the
    corners attrs have all been replaced with the specified SGR."""
    size = GetScreenSize()
    points = [Point(1, 1),
              Point(size.width(), 1),
              Point(size.width(), size.height()),
              Point(1, size.height())]

    attr = esccmd.SGR(esccmd.SGR_BOLD)

    n = 1
    for point in points:
      esccmd.CUP(point)
      escio.Write(str(n))
      n += 1

    esccmd.SGR()

    attrchange = [esccmd.SGR_UNDERLINE, esccmd.SGR_RESET_BOLD]
    self.change(sgr=attrchange)

    required, disallowed = self.compute_changes(attr, attrchange)
    for point in points:
      AssertCellMatchesSGRParams(point, required, disallowed)
      

  @vtLevel(4)
  def changeRectangle_SGRZeroResets(self):
    """Write a value at each corner, run change with SGR 0 arg and default rect args,
    and verify the corners have all been reset."""
    size = GetScreenSize()
    points = [Point(1, 1),
              Point(size.width(), 1),
              Point(size.width(), size.height()),
              Point(1, size.height())]

    attr = esccmd.SGR(esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE)

    n = 1
    for point in points:
      esccmd.CUP(point)
      escio.Write(str(n))
      n += 1

    esccmd.SGR()

    attrchange = [esccmd.SGR_RESET]
    self.change(sgr=attrchange)

    required, disallowed = self.compute_changes(attr, attrchange)
    for point in points:
      AssertCellMatchesSGRParams(point, required, disallowed)

  @vtLevel(4)
  def changeRectangle_respectsOriginMode(self):
    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    # Set margins starting at 2 and 2
    margins = esccmd.SetMargins(2, 2, 9, 9)
    esccmd.SetOriginMode()

    # Change from 1,1 to 3,3 - with origin mode, that's 2,2 to 4,4
    attrchange = [esccmd.SGR_BOLD]
    self.change(top=1,
                left=1,
                bottom=3,
                right=3,
                sgr=attrchange)

    esccmd.ResetMargins()
    esccmd.ResetOriginMode()

    # See what happened.
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(2, 2, 4, 4),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_overlyLargeSourceClippedToScreenSize(self):
    size = GetScreenSize()

    attr = esccmd.SGR(esccmd.SGR_RESET)

    # Put ab, cd in the bottom right
    esccmd.CUP(Point(size.width() - 1, size.height() - 1))
    escio.Write("ab")
    esccmd.CUP(Point(size.width() - 1, size.height()))
    escio.Write("cd")

    esccmd.SGR()

    # Change a 2x2 block starting at the d.
    attrchange = [esccmd.SGR_BOLD]
    self.change(top=size.height(),
                left=size.width(),
                bottom=size.height() + 10,
                right=size.width() + 10,
                sgr=attrchange)
    #AssertScreenCharsInRectEqual(Rect(size.width() - 1,
    #                                  size.height() - 1,
    #                                  size.width(),
    #                                  size.height()),
    #                             ["ab",
    #                              "cd"])
    AssertRectMatchesSGRParams(Rect(size.width() - 1,
                                    size.height() - 1,
                                    size.width(),
                                    size.height()),
                               Rect(size.width(),
                                    size.height(),
                                    size.width(),
                                    size.height()),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_cursorDoesNotMove(self):
    # Make sure something is on screen (so the test is more deterministic)
    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    # Place the cursor
    position = Point(3, 4)
    esccmd.CUP(position)

    # Change a block
    attrchange = [esccmd.SGR_BOLD]
    self.change(top=2,
                left=2,
                bottom=4,
                right=4,
                sgr=attrchange)

    # Make sure the cursor is where we left it.
    AssertEQ(GetCursorPosition(), position)

  @vtLevel(4)
  def changeRectangle_ignoresMargins(self):
    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    margins = esccmd.SetMargins(3, 3, 6, 6)

    attrchange = [esccmd.SGR_BOLD]
    self.change(top=5,
                left=5,
                bottom=7,
                right=7,
                sgr=attrchange)

    esccmd.ResetMargins()

    # Did it ignore the margins?
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 7, 7),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_outsideMarginsInOriginMode(self):
    """Check that a change completely outside the margins is clamped correctly."""
    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    margins = esccmd.SetMargins(2, 2, 5, 5)
    esccmd.SetOriginMode()

    # Change from 5,5 to 8,8 - with origin mode, that's 6,6 to 9,9
    attrchange = [esccmd.SGR_BOLD]
    self.change(top=5,
                left=5,
                bottom=8,
                right=8,
                sgr=attrchange)

    esccmd.ResetMargins()
    esccmd.ResetOriginMode()

    # See what happened.
    #AssertScreenCharsInRectEqual(self.datarect(), self.data())
    AssertRectMatchesSGRParams(self.datarect(),
                               self.changerect(5, 5, 5, 5),
                               self.compute_unchanged(attr, attrchange),
                               self.compute_changes(attr, attrchange))

  @vtLevel(4)
  def changeRectangle_unoccupiedCells(self):
    """The behaviour with erased cells if specific to DECCARA and DECRARA. In
    rectangle mode (DECSACE 2), all cells within the rect are affected; while
    in stream mode (DECSACE 1), erased (unoccupied) cells are *not* affected.
    See DECSACE in DEC STD 070, p. 5-117."""
    attr = esccmd.SGR(esccmd.SGR_RESET)
    self.prepare()
    esccmd.SGR()

    attrchange = [esccmd.SGR_INVERSE, esccmd.SGR_UNDERLINE]
    self.change(top=5,
                left=5,
                bottom=12,
                right=12,
                sgr=attrchange)

    # See what happened.
    if self.decsace() == 2:
      AssertRectMatchesSGRParams(Rect(5, 5, 12, 12),
                                 Rect(5, 5, 12, 12),
                                 [[], []],
                                 [attrchange, []])
    else:
      AssertRectMatchesSGRParams(Rect(5, 5, 12, 12),
                                 Rect(5, 5, 8, 8),
                                 [[], []],
                                 [attrchange, []])
