import esc
import escargs
import esccmd
import escio
from escutil import AssertEQ, AssertTrue, knownBug, vtFirmwareVersion

class DA3Tests(object):
  def handleDA3Response(self):
    [params, str] = escio.ReadDCS("!|")

    reply = "\0\0\0\0"
    if escargs.args.expected_terminal == "vte":
      reply = "~VTE"
    elif escargs.args.expected_terminal == "konsole":
      reply = "~KDE"

    AssertEQ(len(params), 0)

    # Hex-encode the expected reply
    expected_str = ""
    for c in reply:
      expected_str += "%02X" % (ord(c))

    AssertEQ(str, expected_str)

  @vtFirmwareVersion(terminal="vte", minimum=5301)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  def test_DA3_NoParameter(self):
    esccmd.DA3()
    self.handleDA3Response()

  @vtFirmwareVersion(terminal="vte", minimum=5301)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  def test_DA3_0(self):
    esccmd.DA3(0)
    self.handleDA3Response()
