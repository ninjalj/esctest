import esccmd
from tests.save_restore_cursor import SaveRestoreCursorTests
from escutil import knownBug

class DECRCTests(SaveRestoreCursorTests):

  @classmethod
  def saveCursor(cls):
    esccmd.DECSC()

  @classmethod
  def restoreCursor(cls):
    esccmd.DECRC()

  def test_SaveRestoreCursor_InsertNotAffected(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_InsertNotAffected(self)

  def test_SaveRestoreCursor_WorksInLRM(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_WorksInLRM(self, True)

  def test_SaveRestoreCursor_Wrap(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_Wrap(self)
