import tests.decrectopschange
import esccmd
from escutil import knownBug, onlyIn, vtFirmwareVersion

class DECRARATests(tests.decrectopschange.ChangeRectangleTests):

  def __init__(self, decsace):
    tests.decrectopschange.ChangeRectangleTests.__init__(self, decsace)
    self.selftest()

  def change(self, top=None, left=None, bottom=None, right=None, sgr=None):
    esccmd.DECSACE(self.decsace())
    esccmd.DECRARA(top, left, bottom, right, sgr)
    esccmd.DECSACE(0)

  def compute_unchanged(self, attr, changes):
    """ Compute attributes for unchanged cells."""
    required=[]
    disallowed=[]
    for set, reset in esccmd.ALL_SGR:
      if set in attr:
        required += [set]
      elif set in changes:
        disallowed += [set]

    return [required, disallowed]

  def compute_changes(self, attr, changes):
    """ Compute attributes for changed cells. Note this doesn't support
    SGR 30...37, 40...47."""
    required=[code for code in attr if esccmd.SGRCodeIsSet(code)]
    disallowed=[]

    for code in changes:
      if code == esccmd.SGR_RESET:
        for code2 in [esccmd.SGR_BOLD,
                      esccmd.SGR_UNDERLINE,
                      esccmd.SGR_BLINK,
                      esccmd.SGR_INVERSE]:
          if code2 in required:
            required.remove(code2)
            if not code2 in disallowed:
              disallowed += [code2]
          else:
            required += [code2]
            if code2 in disallowed:
              disallowed.remove(code2)

      elif esccmd.SGRCodeIsReset(code): # ignore Reset codes
        continue
      elif esccmd.SGRCodeIsSet(code):
        if code in required:
          required.remove(code)
          if not code in disallowed:
            disallowed += [code]
        else:
          required += [code]
          if code in disallowed:
            disallowed.remove(code)

    return [required, disallowed]

  def selftest(self):
    attr = [esccmd.SGR_BOLD]
    changes = [esccmd.SGR_UNDERLINE, esccmd.SGR_RESET_BOLD]
    assert self.compute_unchanged(attr, changes) == [attr, [esccmd.SGR_UNDERLINE]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE], []]

    attr = [esccmd.SGR_BOLD]
    changes = [esccmd.SGR_UNDERLINE, esccmd.SGR_BOLD]
    assert self.compute_unchanged(attr, changes) == [attr, [esccmd.SGR_UNDERLINE]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_UNDERLINE], [esccmd.SGR_BOLD]]

    attr = changes = [esccmd.SGR_RESET]
    assert self.compute_unchanged(attr, changes) == [[], []]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_BOLD, esccmd.SGR_UNDERLINE, esccmd.SGR_BLINK, esccmd.SGR_INVERSE], []]

    attr = [esccmd.SGR_RESET]
    changes = [esccmd.SGR_RESET, esccmd.SGR_BOLD]
    assert self.compute_unchanged(attr, changes) == [[], [esccmd.SGR_BOLD]]
    assert self.compute_changes(attr, changes) == [[esccmd.SGR_UNDERLINE, esccmd.SGR_BLINK, esccmd.SGR_INVERSE], [esccmd.SGR_BOLD]]

class DECRARARectangleTests(DECRARATests):

  def __init__(self):
    DECRARATests.__init__(self, decsace=esccmd.DECSACE_RECTANGLE)

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_basic(self):
    self.changeRectangle_basic()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_supportedAttributes(self):
    self.changeRectangle_supportedAttributes()

  @onlyIn("vte", "vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_supportedAttributesExtension(self):
    self.changeRectangle_supportedAttributesExtension()

  @onlyIn("vte", "vte extension")
  def test_DECRARA_attributeSetAndReset(self):
    self.changeRectangle_attributeSetAndReset()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECRARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_invalidRectDoesNothing(self):
    self.changeRectangle_invalidRectDoesNothing()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_defaultArgs(self):
    self.changeRectangle_defaultArgs()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_SGRZeroResets(self):
    self.changeRectangle_SGRZeroResets()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_respectsOriginMode(self):
    self.changeRectangle_respectsOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_overlyLargeSourceClippedToScreenSize(self):
    self.changeRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECRARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_cursorDoesNotMove(self):
    self.changeRectangle_cursorDoesNotMove()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_ignoresMargins(self):
    self.changeRectangle_ignoresMargins()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_outsideMarginsInOriginMode(self):
    self.changeRectangle_outsideMarginsInOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_unoccupiedCells(self):
    self.changeRectangle_unoccupiedCells()

class DECRARAStreamTests(DECRARATests):

  def __init__(self):
    DECRARATests.__init__(self, decsace=esccmd.DECSACE_STREAM)

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_basic(self):
    self.changeRectangle_basic()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_supportedAttributes(self):
    self.changeRectangle_supportedAttributes()

  @onlyIn("vte", "vte extension")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_supportedAttributesExtension(self):
    self.changeRectangle_supportedAttributesExtension()

  @onlyIn("vte", "vte extension")
  def test_DECRARA_attributeSetAndReset(self):
    self.changeRectangle_attributeSetAndReset()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECRARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_invalidRectDoesNothing(self):
    self.changeRectangle_invalidRectDoesNothing()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_defaultArgs(self):
    self.changeRectangle_defaultArgs()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_SGRZeroResets(self):
    self.changeRectangle_SGRZeroResets()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_respectsOriginMode(self):
    self.changeRectangle_respectsOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_overlyLargeSourceClippedToScreenSize(self):
    self.changeRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="konsole", reason="DECRARA not implemented.", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_cursorDoesNotMove(self):
    self.changeRectangle_cursorDoesNotMove()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_ignoresMargins(self):
    self.changeRectangle_ignoresMargins()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_outsideMarginsInOriginMode(self):
    self.changeRectangle_outsideMarginsInOriginMode()

  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="konsole", reason="DECRARA not implemented.")
  @knownBug(terminal="wezterm", reason="Not implemented")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECRARA_unoccupiedCells(self):
    self.changeRectangle_unoccupiedCells()
